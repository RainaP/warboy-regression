#!/usr/bin/bash

HOME_PATH=$PWD
SIMFILE="sim.log"

function sim(){
cd $HOME_PATH
	while read TEST_NAME; do
		# Run the simulation
		cd $HOME_PATH/results/${TEST_NAME}
		if [ -e $SIMFILE ];then 
			echo "sim.log exist"
		elif [ ! -e $SIMFILE ];then 
			echo "Restart ${TEST_NAME} Simulation!!"
			srun make sim TEST_NAME=${TEST_NAME} 2>&1 | tee run.log &	
		fi
	done < ./$1
}

#Setting up Environment
source scripts/environment-standalone.sh

#Clean workspace
make clean_all

if [ -d "results" ]; then
	echo "Already exist results folder"
else
	echo "Create results folder"
	mkdir results
fi

while read TEST_NAME; do
	cd $HOME_PATH
	echo $PWD
	if [ -d "results/$TEST_NAME" ]; then
		echo "Already exist $TEST_NAME Path"
		cp -a Makefile scripts results/$TEST_NAME
	else
		echo "Create $TEST_NAME path"
		mkdir results/${TEST_NAME}
		cp -a Makefile scripts results/$TEST_NAME
	fi
done < ./$1

cd $HOME_PATH

#Build C tests
echo "Start C compile!!"
while read TEST_NAME; do
	# Run the simulation
	echo "Start ${TEST_NAME} C compile!!"
	cd $HOME_PATH/results/${TEST_NAME}
	srun make hex TEST_NAME=${TEST_NAME} 2>&1 | tee hex.log &
done < ./$1
echo "C Compile done!"

wait

cd $HOME_PATH

#Build vcs
echo "Start VCS compile!!"
srun make vcs
echo "Compile done!"

while read TEST_NAME; do
	# Run the simulation
	echo "Start ${TEST_NAME} Simulation!!"
	cd $HOME_PATH/results/${TEST_NAME}
	srun make sim TEST_NAME=${TEST_NAME} 2>&1 | tee run.log &
done < ./$1

sleep 10

for ((i=0; i<2; i++));
	do
		sleep 30
		sim ./$1
	done
wait

echo "Done to initiate Tests"
