def AGENT = "uranus"

def configuration() {
    gitlab                       = "portal"
    group                        = "warboy"
    rtl_project                  = "warboy-rtl"
    tb_project                   = "warboy-testbench"
    sim_worksapce                = "semifive_simulation"
    default_branch               = "master"
    test_name                    = "${params.testname}"
}

def module(){
    zshPath                      = "/semifive/tools/Modules/default/init/zsh"
    wit                          = "sifive/wit/0.14.1"
    riscv                        = "riscv-tools/2020.12.8"
    vcs                          = "synopsys/vcs/P-2019.06-SP2"
    designware                   = "synopsys/designware/2019.12-D"
    verdi                        = "synopsys/verdi/P-2019.06-SP2"
}

def do_configuration = configuration()
def do_module = module()

properties properties: [[$class: 'GitLabConnectionProperty', gitLabConnection: 'Portal Gitlab']]

pipeline {
    /*
    *   agent
    */
    agent {
        label "${AGENT}"
    }
    stages {
        stage('Source') {
            steps {
                script{
                    cleanWs()
                    sh """
                        source ${zshPath}
                        module load ${wit}
                        srun -pP2 wit init . -a git@${gitlab}:${group}/${rtl_project}.git -a git@${gitlab}:${group}/${tb_project}.git 
                    """

                }
            }
        }   
        stage('Compile'){
            steps {
                script {
                    sh """
                        source ${zshPath}
                        module load ${riscv}
                        cd ${tb_project}/scripts
                        ./regression_run.sh test_lists/regression_${test_name}.list
                    """
                }
            }
        }
        stage('Build'){
            steps {
                script {
                    sh """
                        source ${zshPath}
                        module load ${vcs}
                        module load ${designware}
                        cd ${rtl_project}/${sim_worksapce}
                        ./regression_run.sh test_lists/regression_${test_name}.list
                    """
                }
            }
        } 
        stage('Check'){
            steps {
                script {
                    sh """
                        cd ${rtl_project}/${sim_worksapce}
                        ./check_result.sh test_lists/regression_${test_name}.list
                    """
                }
            }
        }                            
    }
    post {
        always{
           script{
               archive()
            }
        }
        success {
            script{
                println("success")
           }
        }
        failure {
            script{
                println("failure")
            }
        }
        aborted {
            script{
                println("abort")
            }
        }
    }
}
/*
def checkFailure(){
    println("check failure")
    dir("${rtl_project}/${sim_worksapce}"){
        if(fileExists('sim.log')){
            read = readFile 'sim.log'
            if((read =~ /STATUS: Passed/)){
                println("pass")
            }else{
                sh "exit 1"
            }
        }else{
            println("no sim.log in result directory")
            sh "exit 1"
        }
    }
}*/

def archive(){
    archivePath = "/user/jenkins/archive"
    tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
    tmp_basedir = sh(script: "basename ${tmp_pwd}", returnStdout:true).trim()
    try{
        if (fileExists("${archivePath}/${JOB_NAME}") == false){
            sh "mkdir -p ${archivePath}/${JOB_NAME}"
        }
        sh "srun tar -czf ${archivePath}/${JOB_NAME}/${BUILD_NUMBER}.tar.gz --ignore-failed-read --exclude=.fuse ${tmp_pwd}"
    }catch(e){
        println("archive error")
    }
}
/*
def runSim(){
    list = []
    dir("${rtl_project}/${sim_worksapce}"){
        file = readFile 'regression_tests.list'
        test = file.split('\n')
        for(each in test){
            list.add(each as String)
        }
        total = list.size()  
        for(i = 0; i < list.size(); i++){
            dir("results/${list[i]}"){
                println("Start ${list[i]} Simulation")
                sh "srun ./${list[i]}_run.sh | tee run.log &"
            }
        }
    }
}*/
