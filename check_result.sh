#!/usr/bin/bash

HOME_PATH=$PWD

while read TEST_NAME; do
	cd $HOME_PATH
	if [ -d "results/${TEST_NAME}" ]; then

		if [ -e "results/${TEST_NAME}/status.log" ]; then
			echo -en "${TEST_NAME}\t"
			cat results/${TEST_NAME}/status.log
			echo ""
		else
			echo -en "${TEST_NAME}\t"
			echo "Fail"
		fi
	else
		echo "Can not find ${TEST_NAME}"
	fi
done < ./$1
