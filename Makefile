# Warboy Root
export WARBOY_ROOT = $(shell git rev-parse --show-toplevel)

include ./scripts/setup_path.sh
include ./scripts/riscv_options.sh
include ./scripts/vcs_options.sh

ifeq ($(USE_PIPE_IF),true)
PCIE_SIM_DEFINE = -DPCIE_PIPE_SIM
else
PCIE_SIM_DEFINE = -DPCIE_SERDES_SIM
endif

ifeq ($(LPDDR4_ECC_EN),true)
LPDDR4_ECC_DEFINE = -DENABLE_ECC
endif

export LD_LIBRARY_PATH=${VERI_PATH}/soc-sesame-sifive/api-soctbgen-sifive/templates/models/pcie/furiosa/so_lib:$LD_LIBRARY_PATH

TEST_NAME += $(TEST_NAME)
C_FILE_PATH=${VERI_PATH}/api-soctbgen-sifive/sesame/c/ctests/${C_FILE}.c

load_path :
	$(info WARBOY_ROOT : $(WARBOY_ROOT))
	$(info DESIGN_PATH : $(DESIGN_PATH))
	$(info VERI_PATH : $(VERI_PATH))
	$(info INVENTORY_PATH : $(INVENTORY_PATH))
	$(info FILELISTS_PATH : $(FILELISTS_PATH))
	$(info TESTLISTS_PATH : $(TESTLISTS_PATH))
	$(info SIM_PATH : $(SIM_PATH))
	$(info ***********************************************)
	$(info RISCV_BUILD_OPT : $(RISCV_BUILD_OPT))

#Make elf file
get_c_defines:
	$(eval TEST_C_DEFINES=$(shell python3 scripts/get_test_args.py $(TESTLISTS_PATH) c_defines ${TEST_NAME}))

#Compile C file
elf: get_c_defines
	$(eval C_FILE=$(shell python3 scripts/get_test_args.py $(TESTLISTS_PATH) c_files ${TEST_NAME}))
	riscv64-unknown-elf-gcc  ${RISCV_BUILD_OPT} ${C_FILE_PATH} ${RISCV_COMMON_BUILD_OPT} $(TEST_C_DEFINES) $(PCIE_SIM_DEFINE) $(LPDDR4_ECC_DEFINE)

#Make bin file
bin:
	riscv64-unknown-elf-objcopy -O binary ${TEST_NAME}.elf ${TEST_NAME}.bin

#Make hex file
hex: load_path elf bin
	${VERI_PATH}/api-generator-sifive/scripts/bin2hex --bit-width=32 ${TEST_NAME}.bin ${TEST_NAME}.hex

#Build
vcs:
	vcs ${FILELIST} ${VLOG_BUILD_OPT} ${VCS_OPT} $(XPROP_OPT) $(META_SIM_OPT) $(UPF_SIM_OPT) $(COV_OPT)

#Extract values from json file
get_uvm_testname:
	$(eval UVM_TEST_NAME=$(shell python3 scripts/get_test_args.py $(TESTLISTS_PATH) uvm_test_name ${TEST_NAME}))

get_tilelink_timeout:
	$(eval TILELINK_TIMOUT=$(shell python3 scripts/get_test_args.py $(TESTLISTS_PATH) tilelink_timeout ${TEST_NAME}))

get_max_cycles:
	$(eval MAX_CYCLE=$(shell python3 scripts/get_test_args.py $(TESTLISTS_PATH) max_cycles ${TEST_NAME}))

get_random_seed:
	$(eval RANDOM_SEED=$(shell python3 scripts/get_test_args.py $(TESTLISTS_PATH) random_seed ${TEST_NAME}))

get_plus_args:
	$(eval TEST_PLUS_ARGS=$(shell python3 scripts/get_test_args.py $(TESTLISTS_PATH) test_plus_args ${TEST_NAME}))

#Simulation
sim: get_uvm_testname get_tilelink_timeout get_max_cycles get_random_seed get_plus_args
	${SIM_PATH}/simv ${SIMV_OPT} $(TEST_PLUS_ARGS) $(UVM_TEST_NAME) $(TILELINK_TIMOUT) $(MAX_CYCLE) $(COV_SIM_OPT) 2>&1 | tee sim.log

#Build and Simulation
build_all: hex vcs sim

clean_build: clean_all hex vcs sim

clean_C:
	@\rm -rf *.elf
	@\rm -rf *.bin
	@\rm -rf *.hex
	@\rm -rf *.map
	@\rm -rf *.readelf
	@\rm -rf *.log

clean_VCS:
	@\rm -rf coverage.vdb
	@\rm -rf ucli.key
	@\rm -rf comp.log
	@\rm -rf AN.DB
	@\rm -rf simv.daidir
	@\rm -rf work.lib++
	@\rm -rf elab.log
	@\rm -rf simv
	@\rm -rf simv.vdb
	@\rm -rf csrc
	@\rm -rf vc_hdrs.h
	@\rm -rf *.log
	@\rm -rf *.fsdb
	@\rm -rf *.mempa
	@\rm -rf *.xml
	@\rm -rf verdi_config_file
	@\rm -rf results

clean_all: clean_C clean_VCS
