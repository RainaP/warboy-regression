import os
import sys
import shutil
import json
import string
import random
import fnmatch

def load_json_from_file(filename=str):
    name = filename.strip()
    if not os.path.isfile(name):
        print("Could not open file: " + name + " for reading")
        exit(1)
    with open(name, "r", encoding="utf-8", errors="ignore") as fp:
        return json.load(fp)

#Create random number whenever the script run.
_LENGTH = 5
string_pool = string.digits
random_str = ""
for i in range(_LENGTH) :
    random_str += random.choice(string_pool)

path = sys.argv[1]
test_found = 0
flag_base_test = 0

for test_list_json in os.listdir(path):
   if ".swp" in test_list_json:
       continue
   if "upf" in test_list_json:
       continue
   if "json" in test_list_json:
       test_list = load_json_from_file(path+test_list_json)
   else:
       continue
   for test in test_list['tests']:
      if test['name'] == sys.argv[3]:
         if sys.argv[2] == "c_files":
             if 'c_files' in test.keys():
                 if sys.argv[2] == "c_files":
                     c_files = " "
                     for args in test['c_files']:
                         c_files =  c_files+args
                     print(c_files)                                      
             else:
                 print(sys.argv[3])

         if sys.argv[2] == "uvm_test_name":
            if test['uvm_test_name'] == 'base_test':
                print("+UVM_TESTNAME=sesame_base_test")
            else:
                print("+UVM_TESTNAME=sesame_"+test['uvm_test_name'])

         if sys.argv[2] == "tilelink_timeout":
            tilelink_timeout = " +tilelink_timeout="
            for args in test['tilelink_timeout']:
                tilelink_timeout =  tilelink_timeout+args
            print(tilelink_timeout)

         if fnmatch.fnmatch(test['name'],"I2C*") == True:
            if sys.argv[2] == "max_cycles":
                print(" ")
         else:
            if sys.argv[2] == "max_cycles":
                max_cycles = " +max_cycles="
                for args in test['max_cycles']:
                    max_cycles =  max_cycles+args
                print(max_cycles)

         if sys.argv[2] == "random_seed":
            random_seed = " +ntb_random_seed="
            for args in test['random_seed']:
                random_seed =  random_seed+args
            print(random_seed)

         if sys.argv[2] == "test_plus_args":
            test_plus_args = ""
            for args in test['test_plus_args']:
               test_plus_args =  test_plus_args+" +"+args
            print(test_plus_args)

         if sys.argv[2] == "c_defines":
            c_defines = ""
            for args in test['c_defines']:
                c_defines = c_defines+" "+args
            print(c_defines)

         test_found = 1
         break
   if test_found==1:
       break
